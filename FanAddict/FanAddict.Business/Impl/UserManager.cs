﻿using FanAddict.Entity;
using FanAddict.Repository;

namespace FanAddict.Business.Impl
{
    public class UserManager : IUserManager
    {
        private IUserRepository _userRepository;
        public UserManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public Entity.UserInformation Signin(string email, string password)
        {
            Repository.Data.UserInformation userinfo = _userRepository.Signin(email, password);
            if (userinfo != null)
            {
                return new Entity.UserInformation()
                {
                    EmailId = userinfo.EmailId,
                    FirstName = userinfo.FirstName,
                    Id = userinfo.Id,
                    LastName = userinfo.LastName,
                    Password = userinfo.Password,
                    Phone = userinfo.Phone
                };
            }
            else
            {
                return null;
            }
        }

        public SignupResponse Signup(Entity.UserInformation userInformation)
        {
            SignupResponse response = new SignupResponse();
            Repository.Data.UserInformation userinfo = _userRepository.Signup(new Repository.Data.UserInformation()
            {
                EmailId = userInformation.EmailId,
                FirstName = userInformation.FirstName,
                Id = userInformation.Id,
                LastName = userInformation.LastName,
                Password = userInformation.Password,
                Phone = userInformation.Phone
            });
            if (userinfo == null)
            {
                response.Status = false;
            }
            else
            {
                response.Status = true;
                response.UserId = userinfo.Id.ToString();
            }
            return response;
        }
    }
}
