﻿using FanAddict.Entity;
using FanAddict.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanAddict.Business
{
    public interface IUserManager
    {
        Entity.UserInformation Signin(string email, string password);

        SignupResponse Signup(Entity.UserInformation userInformation);
    }
}
