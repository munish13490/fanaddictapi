﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanAddict.Entity
{
    public class SignupResponse
    {
        public bool Status { get; set; }
        public string UserId { get; set; }
    }
}
