﻿using FanAddict.Business;
using FanAddict.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FanAddict.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private IUserManager _userManager;
        public UserController(IUserManager userManager)
        {
            _userManager = userManager;
        }

        [HttpGet]
        [Route("signin")]
        public UserInformation Signin(string email, string password)
        {
            return _userManager.Signin(email, password);
        }

        [HttpPost]
        [Route("signup")]
        public SignupResponse Signup(UserInformation userInformation)
        {
            return _userManager.Signup(userInformation);
        }
    }
}
