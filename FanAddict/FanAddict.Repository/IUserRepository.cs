﻿using FanAddict.Repository.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanAddict.Repository
{
    public interface IUserRepository
    {
        UserInformation Signin(string email, string password);

        UserInformation Signup(UserInformation userInformation);
    }
}
