﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FanAddict.Repository.Data;

namespace FanAddict.Repository.Impl
{
    public class UserRepository : IUserRepository
    {
        FanAddictEntities fanAddictEntities;
        public UserRepository()
        {
            fanAddictEntities = new FanAddictEntities();
        }
        public UserInformation Signin(string email, string password)
        {
            return fanAddictEntities.UserInformations.FirstOrDefault(x => x.EmailId == email && x.Password == password);
        }

        public UserInformation Signup(UserInformation userInformation)
        {
            var user = fanAddictEntities.UserInformations.FirstOrDefault(x => x.EmailId == userInformation.EmailId);
            if (user != null)
            {
                return null;
            }
            else
            {
                userInformation.Id = Guid.NewGuid();
                userInformation = fanAddictEntities.UserInformations.Add(userInformation);
                fanAddictEntities.SaveChanges();
                return userInformation;
            }
        }
    }
}
