﻿using FanAddict.Business;
using FanAddict.Business.Impl;
using Ninject.Modules;

namespace FanAddict.DependencyResolver
{
    public class BusinessLogic : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserManager>().To<UserManager>().InSingletonScope();
        }
    }
}
