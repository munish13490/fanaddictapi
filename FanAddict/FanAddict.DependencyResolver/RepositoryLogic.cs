﻿using FanAddict.Repository;
using FanAddict.Repository.Impl;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FanAddict.DependencyResolver
{
    public class RepositoryLogic : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserRepository>().To<UserRepository>().InSingletonScope();
        }
    }
}
